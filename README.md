# release-certification-tools
This is a script that will generate a public issue in the GitLab repository with the information required to perform an AppSec certification of a release. More information about the AppSec release certification process can be found [here](https://about.gitlab.com/handbook/ceo/chief-of-staff-team/jihu-support/release-certification.html).

## Description
Running this script will perform the following actions:

- Find the upcoming [release task](https://gitlab.com/gitlab-org/release/tasks/-/issues) via the GitLab API
- Find all JiHu Contributions to [each repository](https://about.gitlab.com/handbook/ceo/chief-of-staff-team/jihu-support/#projects) for the upcoming release (**NOTE**: each MR must be labeled with `JiHu Contribution` and be associated with the same milestone as the release task)
- Create an AppSec Release Certification Issue in the [jh-upstream-reports repository](https://gitlab.com/gitlab-org/jh-upstream-report/-/issues?sort=created_date&state=opened) with a checklist containing each JiHu contribution associated with the upcoming release

The [jh-upstream-reports repository](https://gitlab.com/gitlab-org/jh-upstream-report) runs this script via scheduled pipelines for the monthly releases.

## Contributing
Any changes made to this repository will impact the [scheduled pipelines being run by the jh-upstream-reports repository](https://gitlab.com/gitlab-org/jh-upstream-report/-/pipeline_schedules). In order to make sure these pipelines do not break and disrupt the release certification process, please be sure to test that the pipelines still function correctly. To test that everything still works:

* [Manually run a pipeline in the jh-upstream-reports repository](https://gitlab.com/gitlab-org/jh-upstream-report/-/pipelines), which should create a new certification issue in the [jh-upstream-reports issue tracker](https://gitlab.com/gitlab-org/jh-upstream-report/-/issues?sort=created_date&state=opened)
* Verify that the certification issue was created correctly and (if appropriate) that your expected changes worked
* Change the title of the certification issue that was created to `TEST PLEASE IGNORE`
* Remove the associated milestone from the release certification issue
* Close the release certification issue

## Requirements
- You will need a [GitLab Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with the `api` scope
- The access token needs to be set in a `GITLAB_PAT` environment variable (`export GITLAB_PAT="token"`) or it can be provided inline when running the script (`GITLAB_PAT="token" ruby create_checklist.rb`)
    - Given the required scope of the token and the damages leaking it would cause, it's recommended to create a token specifically for each release certification and set it to expire on the next day.

## Usage
This script can be used to create certification checklists for both monthly releases and patch releases. To begin, you will need to get everything set up:
- Clone this repository
- From the root directory, install the dependencies via `bundle install` (you may need to `gem install bundler` first)

### To create a release certification issue for the next upcoming monthly release:
- Run the scipt with `ruby create_checklist.rb`
- The output will include a link to the newly created AppSec Release Certification issue

### To create a release certification issue for a specific monthly release:
- Locate the release task issue and make note of the issue id (ex `https://gitlab.com/gitlab-org/release/tasks/-/issues/<ID>`)
- Run the scipt with `ruby create_checklist.rb -t 1234` (where `1234` is the id of the release issue)
- The output will include a link to the newly created AppSec Release Certification issue

### To check if a particular patch release has any JiHu contributions:
- Locate the patch release task issue and make note of the issue id (ex `https://gitlab.com/gitlab-org/release/tasks/-/issues/<ID>`)
- Run the script with `-c` as well as `-t` followed by the patch release issue id: `ruby create_checklist -c -t 1234` (where `1234` is the id of the release issue)
- The output will indicate if any JiHu contributions were included in this patch

### To create a release certification issue for a particular patch release:
- Locate the patch release task issue and make note of the issue id (ex `https://gitlab.com/gitlab-org/release/tasks/-/issues/<ID>`)
- Run the script with `-p` as well as `-t` followed by the patch release issue id: `ruby create_checklist -p -t 1234` (where `1234` is the id of the release issue)
- The output will include a link to the newly created AppSec Release Certification issue

# Merge Request Monitor
The Merge Request Monitor will check the public repositories that JiHu contributes to for any `merged` Merge Requests that are labeled as JiHu Contributions but did not receive the `sec-planning::complete` label. Any merge requests found will be submitted as issues to the [jihu_merge_request_monitor_reports repository](https://gitlab.com/gitlab-com/gl-security/appsec/jihu_merge_request_monitor_reports). This will be run via a scheduled pipeline in that repository.
