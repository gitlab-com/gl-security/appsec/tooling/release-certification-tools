# frozen_string_literal: true

HEADERS = {
  'Private-Token' => ENV['GITLAB_PAT']
}

API_URL = 'https://gitlab.com/api/v4'.freeze
JIHU_LABEL_NAME = 'JiHu%20contribution'.freeze
SECURITY_REVIEW_COMPLETE_LABEL_NAME = 'sec-planning::complete'.freeze

RELEASE_TASK_PROJECT_ID = '5064907'.freeze

# List of projects JiHu might contribute to https://about.gitlab.com/handbook/ceo/chief-of-staff-team/jihu-support/#projects
GITLAB_PROJECT_ID = '278964'.freeze
CUSTOMERS_PROJECT_ID = '2670515'.freeze
VERSION_PROJECT_ID = '43521838'.freeze
OMNIBUS_PROJECT_ID = '20699'.freeze
ENVIRONMENT_TOOLKIT_PROJECT_ID = '14292404'.freeze
CNG_PROJECT_ID = '4359271'.freeze
CHARTS_PROJECT_ID = '3828396'.freeze
WWW_GITLAB_COM_PROJECT_ID = '7764'.freeze
DOCS_PROJECT_ID = '1794617'.freeze
RUNNER_PROJECT_ID = '250833'.freeze
GITALY_PROJECT_ID = '2009901'.freeze
GITLAB_SVG_PROJECT_ID = '4149988'.freeze
GITLAB_QA_PROJECT_ID = '1441932'.freeze

ALL_PROJECT_IDS = [
  GITLAB_PROJECT_ID,
  OMNIBUS_PROJECT_ID,
  ENVIRONMENT_TOOLKIT_PROJECT_ID,
  CNG_PROJECT_ID,
  CHARTS_PROJECT_ID,
  WWW_GITLAB_COM_PROJECT_ID,
  DOCS_PROJECT_ID,
  RUNNER_PROJECT_ID,
  GITALY_PROJECT_ID,
  GITLAB_SVG_PROJECT_ID,
  VERSION_PROJECT_ID,
  GITLAB_QA_PROJECT_ID
]

PRIVATE_PROJECT_IDS = [
  CUSTOMERS_PROJECT_ID
]

PROJECT_DISPLAY_NAME_FOR = {
  GITLAB_PROJECT_ID              => '[GitLab](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?scope=all&state=opened&label_name[]=JiHu%20contribution)',
  CUSTOMERS_PROJECT_ID           => '[Customers](https://gitlab.com/gitlab-org/customers-gitlab-com/-/merge_requests?scope=all&state=opened&label_name[]=JiHu%20contribution)',
  VERSION_PROJECT_ID             => '[Version](https://gitlab.com/gitlab-org/gitlab-services/version.gitlab.com/-/merge_requests?scope=all&state=opened&label_name[]=JiHu%20contribution)',
  OMNIBUS_PROJECT_ID             => '[Omnibus](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests?scope=all&state=opened&label_name[]=JiHu%20contribution)',
  ENVIRONMENT_TOOLKIT_PROJECT_ID => '[GitLab Environment Toolkit](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit/-/merge_requests?scope=all&state=opened&label_name[]=JiHu%20contribution)',
  CNG_PROJECT_ID                 => '[CNG](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests?scope=all&state=opened&label_name[]=JiHu%20contribution)',
  CHARTS_PROJECT_ID              => '[Charts](https://gitlab.com/gitlab-org/charts/gitlab/-/merge_requests?scope=all&state=opened&label_name[]=JiHu%20contribution)',
  WWW_GITLAB_COM_PROJECT_ID      => '[www-GitLab-com](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests?scope=all&state=opened&label_name[]=JiHu%20contribution)',
  DOCS_PROJECT_ID                => '[GitLab-Docs](https://gitlab.com/gitlab-org/gitlab-docs/-/merge_requests?scope=all&state=opened&label_name[]=JiHu%20contribution)',
  RUNNER_PROJECT_ID              => '[GitLab Runner](https://gitlab.com/gitlab-org/gitlab-runner/-/merge_requests?scope=all&state=opened&label_name[]=JiHu%20contribution)',
  GITALY_PROJECT_ID              => '[Gitaly](https://gitlab.com/gitlab-org/gitaly/-/merge_requests?scope=all&state=opened&label_name[]=JiHu%20contribution)',
  GITLAB_SVG_PROJECT_ID          => '[gitlab-svgs](https://gitlab.com/gitlab-org/gitlab-svgs/-/merge_requests?scope=all&state=opened&label_name[]=JiHu%20contribution)',
  GITLAB_QA_PROJECT_ID           => '[gitlab-qa](https://gitlab.com/gitlab-org/gitlab-qa/-/merge_requests?scope=all&state=opened&label_name[]=JiHu%20contribution)'
}

PROJECT_NAME_FOR = {
  GITLAB_PROJECT_ID              => 'GitLab',
  CUSTOMERS_PROJECT_ID           => 'Customers',
  VERSION_PROJECT_ID             => 'Version',
  OMNIBUS_PROJECT_ID             => 'Omnibus',
  ENVIRONMENT_TOOLKIT_PROJECT_ID => 'GitLab Environment Toolkit',
  CNG_PROJECT_ID                 => 'CNG',
  CHARTS_PROJECT_ID              => 'Charts',
  WWW_GITLAB_COM_PROJECT_ID      => 'www-GitLab-com',
  DOCS_PROJECT_ID                => 'GitLab-Docs',
  RUNNER_PROJECT_ID              => 'GitLab Runner',
  GITALY_PROJECT_ID              => 'Gitaly',
  GITLAB_SVG_PROJECT_ID          => 'gitlab-svgs',
  GITLAB_QA_PROJECT_ID           => 'gitlab-qa'
}

PROJECT_NOTE_FOR = {
}

# The project the release certification issue will be created in
UPSTREAM_REPORT_PROJECT_ID = '28636453'.freeze
RELEASE_CERTIFICATION_PROJECT_ID = UPSTREAM_REPORT_PROJECT_ID

MERGE_MONITOR_REPORT_PROJECT_ID = '34358474'.freeze

DOCUMENTATION_MESSAGE = 'For more information about this process and what is expected for this issue, please refer to [the documentation](https://about.gitlab.com/handbook/ceo/chief-of-staff-team/jihu-support/release-certification.html).'.freeze
CONTRIBUTIONS_LIST_MESSAGE = 'Below is a list of JiHu contributions that were identified in the [GitLab projects they contribute to](https://about.gitlab.com/handbook/ceo/chief-of-staff-team/jihu-support/#projects). Be sure to check that this list is up-to-date and that all JiHu contributions have been included.'.freeze
PATCH_RELEASE_CONTRIBUTIONS_LIST_MESSAGE = 'Below is a list of JiHu contributions that were identified in the projects that are part of the patch release process. Be sure to check that this list is up-to-date and that all JiHu contributions have been included.'.freeze
NO_CONTRIBUTIONS_MESSAGE = 'No contributions found for this project for this release.'.freeze

MANUAL_REVIEW_HEADER = '## Repositories requiring manual review (:exclamation:)'
MANUAL_REVIEW_DESCRIPTION = "The following repositories will require manual review, since they or their merge request sections are private and inaccessible by the automation's project access token. Please see the [certification process documentation](https://about.gitlab.com/handbook/ceo/chief-of-staff-team/jihu-support/release-certification.html#certification-process) for more information."
MANUAL_REVIEW_INSTRUCTIONS = 'For each repository listed below, visit the merge request section and search for any MRs that are labeled ~\"JiHu contribution\". Note that these may be `Merged` and may not necessarily be related to the current release milestone. If no MRs labeled ~\"JiHu contribution\" are present, check the box indicating that no JiHu contributions were made in this release. If any MRs labeled ~\"JiHu contribution\" are present, please follow the normal process to verify that it received an AppSec review and were labeled ~\"sec-planning::complete\". Please also add a checkbox under each repository section and be sure to check it if the ~\"sec-planning::complete\" was applied.'
MANUAL_REVIEW_DEFAULT_CHECKBOX = '- [ ] This repository was manually reviewed for any merge requests labeled ~\"JiHu contribution\"'
MANUAL_REVIEW_NO_MERGE_REQUESTS_CHECKBOX = '- [ ] No ~\"JiHu contribution\" merge requests were included as part of this release or merged in the last month'

RELEASE_CERTIFICATION_DOCUMENTATION_URL = 'https://handbook.gitlab.com/handbook/ceo/chief-of-staff-team/jihu-support/release-certification/'.freeze
FEDERAL_APPSEC_TEAM_ALIAS = '@gitlab-com/gl-security/product-security/federal-application-security'.freeze
