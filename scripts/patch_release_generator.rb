require 'httparty'

require_relative 'constants'
require_relative 'utils'

class PatchReleaseGenerator
  def self.create_checklist(options)
    if options[:check_patch_release]
      check_patch_release_for_jihu_contributions(options)
    elsif options[:from_automation]
      puts 'Running via scheduled pipeline....'
      run_from_scheduled_pipeline(options)
    else
      puts 'Creating patch release checklist...'
      generate_patch_release_issue(options)
    end
  end

  def self.generate_patch_release_issue(options)
    release_task = Utils.retrieve_specific_task_issue(options[:task_id])
    jihu_contributions = Utils.identify_jihu_contributions_from_blog_diff(options[:task_id])

    create_patch_release_certification_issue(release_task, jihu_contributions, options)
  end

  def self.run_from_scheduled_pipeline(options)
    task_issues = find_recent_patch_release_task_issues
    deduped_task_issues = dedupe_previous_patch_release_certifications(task_issues)
    deduped_task_issues.each do |task_issue|
      jihu_contributions = Utils.identify_jihu_contributions_from_blog_diff(task_issue['iid'])
      create_patch_release_certification_issue(task_issue, jihu_contributions, options)
    end
  end

  def self.find_recent_patch_release_task_issues
    url = "#{API_URL}/projects/#{RELEASE_TASK_PROJECT_ID}/issues"
    response = HTTParty.get(url, headers: HEADERS, query: find_patch_release_issue_request_query)
    response.select { |i| Utils.title_matches_patch_release(i['title']) }
  end

  def self.find_patch_release_issue_request_query
    last_run_date = Date.today.prev_month.prev_day.strftime("%FT%T")
    {
      'created_after' => last_run_date,
      'state' => 'closed',
      'labels' => 'Monthly Release',
      'not' => {
        'labels' => ['security', 'QA task']
      }
    }
  end

  def self.dedupe_previous_patch_release_certifications(task_issues)
    url = "#{API_URL}/projects/#{RELEASE_CERTIFICATION_PROJECT_ID}/issues"
    response = HTTParty.get(url, headers: HEADERS, query: recent_certification_issues_query)
    certification_titles = response.map { |i| i['title'] }
    task_issues.reject do |task_issue|
      certification_titles.any? { |c| c.include?(task_issue['title']) }
    end
  end

  def self.recent_certification_issues_query
    last_run_date = Date.today.prev_month.prev_day.strftime("%FT%T")
    { 'created_after' => last_run_date }
  end

  def self.identify_jihu_contributions_from_notes(task_id)
    merge_request_notes = retrieve_merge_requests_notes(task_id)
    repository_mapping = sort_merge_request_notes_by_repository(merge_request_notes)

    gitlab_jihu_contributions = retrieve_all_jihu_contributions(repository_mapping[:gitlab][:note], GITLAB_PROJECT_ID)
    omnibus_jihu_contributions = retrieve_all_jihu_contributions(repository_mapping[:omnibus][:note], OMNIBUS_PROJECT_ID)
    cng_jihu_contributions = retrieve_all_jihu_contributions(repository_mapping[:cng][:note], CNG_PROJECT_ID)
    gitaly_jihu_contributions = retrieve_all_jihu_contributions(repository_mapping[:gitaly][:note], GITALY_PROJECT_ID)

    {
      GITLAB_PROJECT_ID  => gitlab_jihu_contributions,
      OMNIBUS_PROJECT_ID => omnibus_jihu_contributions,
      CNG_PROJECT_ID => cng_jihu_contributions,
      GITALY_PROJECT_ID => gitaly_jihu_contributions
    }
  end




  def self.retrieve_all_jihu_contributions(id_array, project_id)
    return id_array if id_array.empty?
    base_url = "#{API_URL}/projects/#{project_id}/merge_requests?iids[]="
    id_array.each_with_index do |id, idx|
      if id_array[idx] == id_array.last
        base_url << id
      else
        base_url << "#{id}&iids[]="
      end
    end
    merge_requests = HTTParty.get(base_url, headers: HEADERS).parsed_response
    merge_requests.inject([]) do |acc, mr|
      acc << mr if mr["labels"].include?('JiHu contribution')
      acc
    end
  end

  def self.sort_merge_request_notes_by_repository(merge_request_notes)
    repository_mapping = {
      gitlab: { note: [] },
      omnibus: { note: [] },
      cng: { note: [] },
      gitaly: { note: [] }
    }
    merge_request_notes.each do |n|
      mr_links = n['body'].split('*').inject([]) { |acc, a| acc << a.scan(/https?:\/\/[\S]+/) }.flatten
      mr_links.each do |link|
        if link[/gitlab-org\/gitlab/]
          repository_mapping[:gitlab][:note] << link[/\d+/]
        elsif link[/gitlab-org\/gitaly/]
          repository_mapping[:gitaly][:note] << link[/\d+/]
        elsif link[/gitlab-org\/omnibus-gitlab/]
          repository_mapping[:omnibus][:note] << link[/\d+/]
        elsif link[/gitlab-org\/build\/CNG/]
          repository_mapping[:cng][:note] << link[/\d+/]
        end
      end
    end
    repository_mapping
  end

  def self.retrieve_merge_requests_notes(task_id)
    url = "#{API_URL}/projects/#{RELEASE_TASK_PROJECT_ID}/issues/#{task_id}/discussions?per_page=100"
    task_discussions = HTTParty.get(url, headers: HEADERS).parsed_response
    picked_into_discussions = task_discussions.select { |p| p.dig('notes').detect { |y| y['body'].include?("The following merge requests were picked into") } }
    merge_request_notes = picked_into_discussions.inject([]) do |acc, discussion|
      acc << discussion['notes']
    end
    merge_request_notes.flatten!
  end

  def self.create_patch_release_certification_issue(release_task, jihu_contributions, options)
    url = "#{API_URL}/projects/#{RELEASE_CERTIFICATION_PROJECT_ID}/issues"
    issue_description = build_description(jihu_contributions, release_task['web_url'])
    params = Utils.create_release_certification_issue_params(release_task, issue_description, nil)
    response = HTTParty.post(url, headers: HEADERS, query: params)
    certification_issue_url = response.parsed_response['web_url']
    Utils.append_data_to_issue_description(response.parsed_response['iid'], certification_issue_url, issue_description)
  end

  def self.build_description(jihu_contributions, release_task_url)
    description = Utils.generate_certification_checklist_issue_header(release_task_url)
    description << "#{PATCH_RELEASE_CONTRIBUTIONS_LIST_MESSAGE}\n\n"

    jihu_contributions.each do |project_id, contributions|
      description << "### #{PROJECT_DISPLAY_NAME_FOR[project_id]}\n"
      if contributions.length < 1
        description << "#{NO_CONTRIBUTIONS_MESSAGE}\n\n"
      else
        contributions.each do |c|
          description << Utils.build_contribution_display(c)
        end
      end
    end
    description
  end

  def self.check_patch_release_for_jihu_contributions(options)
    task_id = options[:task_id]
    puts "Checking for JiHu contributions for patch release task id #{task_id}..."
    jihu_contributions = Utils.identify_jihu_contributions_from_blog_diff(task_id)
    display_contribution_check_results(jihu_contributions)
  end

  def self.display_contribution_check_results(jihu_contributions)
    if jihu_contributions.any? { |k,v| v.length > 0 }
      count = jihu_contributions.values.flatten.length
      puts "... #{count} JiHu Contributions found. Please certify the release."
    else
      puts '... no JiHu contributions found. Release certification may not be necessary.'
    end
  end
end
