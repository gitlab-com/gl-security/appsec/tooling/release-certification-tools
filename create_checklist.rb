require_relative 'scripts/checklist_generator'
require_relative 'scripts/patch_release_generator'
require 'optparse'

options = {}

option_parser = OptionParser.new do |opts|
  opts.on('-t', '--task 12345', '[OPTIONAL | REQUIRED for patch releases and --check] id for a specific release task') do |t|
    options[:task_id] = t
  end

  opts.on('-p', '--patch', '[OPTIONAL] Follow the patch release process') do |p|
    options[:patch_release] = true
  end

  opts.on('-c', '--check', '[OPTIONAL] Check if a patch release contains any JiHu Contributions') do |p|
    options[:patch_release] = true
    options[:check_patch_release] = true
  end

  opts.on('-a', '--automation', '[OPTIONAL] For running the patch release checklist creation via scheduled pipeline') do |p|
    options[:patch_release] = true
    options[:from_automation] = true
  end
end

option_parser.parse!

if options[:patch_release]
  PatchReleaseGenerator.create_checklist(options)
else
  ChecklistGenerator.create_checklist(options)
end
